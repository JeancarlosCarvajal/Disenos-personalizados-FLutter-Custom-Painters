# custom_painter

Disenos personalizados FLutter Custom Painters para Headers

## Appearance Application

![Appearance 1](appearance/11.png)
![Appearance 2](appearance/22.png)
![Appearance 3](appearance/33.png)
![Appearance 4](appearance/44.png)
![Appearance 5](appearance/55.png)
![Appearance 6](appearance/66.png)
![Appearance 7](appearance/77.png)
![Appearance 8](appearance/88.png)
![Appearance 9](appearance/99.png)
 